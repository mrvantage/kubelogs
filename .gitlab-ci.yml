.build_template: &build_template
  stage: build
  script:
    - GOOS=${GOOS} GOARCH=${GOARCH} go build ./cmd/kubelogs.go
    - sha256sum ${ARTIFACT_NAME} > ${ARTIFACT_NAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}.sha256
    - tar -czvf ${ARTIFACT_NAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}.tgz ${ARTIFACT_NAME}
  artifacts:
    paths:
      - ${ARTIFACT_NAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}.tgz
      - ${ARTIFACT_NAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}.sha256
  only:
    - tags
  except:
    - branches

.upload_template: &upload_template
  stage: upload
  image: curlimages/curl:latest
  script:
    - |
      curl -v --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${ARTIFACT_NAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}.tgz \
      "${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_${GOOS}_${GOARCH}/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}.tgz"
    - |
      curl -v --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${ARTIFACT_NAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}.sha256 \
      "${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_${GOOS}_${GOARCH}/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}.sha256"
  only:
    - tags
  except:
    - branches

default:
  image: golang:1.17

variables:
  ARTIFACT_NAME: "kubelogs"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"

stages:
  - test
  - build
  - upload
  - release

unit:
  stage: test
  script:
    - go install github.com/gregoryv/uncover/cmd/uncover@latest
    - go test -cover -coverprofile coverage.out ./...
    # - uncover -min 100.0 coverage.out
  artifacts:
    paths:
      - coverage.out
  only:
    - merge_requests
    - tags

lint:
  stage: test
  script:
    - go install golang.org/x/lint/golint@latest
    # - golint -set_exit_status ./...
  only:
    - merge_requests
    - tags

build_linux_amd64:
  <<: *build_template           # Merge the contents of the 'job_configuration' alias
  variables:
    GOOS: linux
    GOARCH: amd64

build_linux_386:
  <<: *build_template           # Merge the contents of the 'job_configuration' alias
  variables:
    GOOS: linux
    GOARCH: 386

build_linux_arm64:
  <<: *build_template           # Merge the contents of the 'job_configuration' alias
  variables:
    GOOS: linux
    GOARCH: arm64

build_linux_arm:
  <<: *build_template           # Merge the contents of the 'job_configuration' alias
  variables:
    GOOS: linux
    GOARCH: arm

upload_linux_amd64:
  <<: *upload_template           # Merge the contents of the 'job_configuration' alias
  variables:
    GOOS: linux
    GOARCH: amd64

upload_linux_386:
  <<: *upload_template           # Merge the contents of the 'job_configuration' alias
  variables:
    GOOS: linux
    GOARCH: 386

upload_linux_arm64:
  <<: *upload_template           # Merge the contents of the 'job_configuration' alias
  variables:
    GOOS: linux
    GOARCH: arm64

upload_linux_arm:
  <<: *upload_template           # Merge the contents of the 'job_configuration' alias
  variables:
    GOOS: linux
    GOARCH: arm

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/'
  script:
    - echo "Making release ${CI_COMMIT_TAG}"
    - |
      release-cli create \
      --name "Release $CI_COMMIT_TAG" \
      --description './CHANGELOG.md' \
      --tag-name "$CI_COMMIT_TAG" \
      --ref "$CI_COMMIT_SHA" \
      --assets-link="{\"name\": \"${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_amd64.tgz\", \"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_linux_amd64/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_amd64.tgz\", \"type\": \"other\"}" \
      --assets-link="{\"name\": \"${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_amd64.sha256\", \"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_linux_amd64/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_amd64.sha256\", \"type\": \"other\"}" \
      --assets-link="{\"name\": \"${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_386.tgz\", \"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_linux_386/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_386.tgz\", \"type\": \"other\"}" \
      --assets-link="{\"name\": \"${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_386.sha256\", \"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_linux_386/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_386.sha256\", \"type\": \"other\"}" \
      --assets-link="{\"name\": \"${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_arm64.tgz\", \"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_linux_arm64/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_arm64.tgz\", \"type\": \"other\"}" \
      --assets-link="{\"name\": \"${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_arm64.sha256\", \"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_linux_arm64/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_arm64.sha256\", \"type\": \"other\"}" \
      --assets-link="{\"name\": \"${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_arm.tgz\", \"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_linux_arm/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_arm.tgz\", \"type\": \"other\"}" \
      --assets-link="{\"name\": \"${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_arm.sha256\", \"url\":\"${PACKAGE_REGISTRY_URL}/${ARTIFACT_NAME}_linux_arm/${CI_COMMIT_TAG}/${ARTIFACT_NAME}_${CI_COMMIT_TAG}_linux_arm.sha256\", \"type\": \"other\"}"

