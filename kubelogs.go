package kubelogs

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"regexp"
	"time"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	"github.com/logrusorgru/aurora"
)

var Debug bool

func init() {
	Debug = false
}

func debug(message string) {
	if Debug {
		log.Println(message)
	}
}

type KubeLogs struct {
	config        *rest.Config
	clientset     *kubernetes.Clientset
	activeLoggers []ContainerID
}

func (k *KubeLogs) LoadConfigFromFile(file string) error {
	var err error
	k.config, err = clientcmd.BuildConfigFromFlags("", file)
	return err
}

func (k *KubeLogs) LoadClientSet() error {
	var err error
	k.clientset, err = kubernetes.NewForConfig(k.config)
	return err
}

func (k *KubeLogs) WatchPods(namespace string, interval time.Duration, results chan PodList) error {
	for {
		debug("Retrieving pods")
		pods, err := k.clientset.CoreV1().Pods(namespace).List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			return err
		}

		results <- (PodList)(pods.Items)

		time.Sleep(interval)

	}
}

func (k *KubeLogs) GetContainerLogs(id ContainerID, tail int64, follow bool) error {

	debug(fmt.Sprintf("Starting logger for %#v", id))

	if k.LoggerActive(id) {
		debug(fmt.Sprintf("Logger already active %#v", id))
		return nil
	}

	k.activeLoggers = append(k.activeLoggers, id)

	index := k.LoggerIndex(id)
	color := uint8((index % 6) + 1)
	log.Printf("Attaching to %s/%s\n", aurora.Index(color, id.PodName), aurora.Index(color, id.ContainerName))

	podLogOptions := v1.PodLogOptions{
		Container: id.ContainerName,
		Follow:    follow,
		TailLines: &tail,
	}

	// Attach to the loggers log stream
	podLogRequest := k.clientset.CoreV1().Pods(id.Namespace).GetLogs(id.PodName, &podLogOptions)
	stream, err := podLogRequest.Stream(context.TODO())
	if err != nil {
		debug(fmt.Sprintf("Could not get stream for %#v %s", id, err))
		return err
	}
	defer stream.Close()

	// Convert to a bufio.Reader to be able to read line by line.
	b := bufio.NewReader(stream)
	line := make([]byte, 0)
	for {

		// Read line, or up to buffer size
		r, isPrefix, err := b.ReadLine()
		if err != nil {
			debug(fmt.Sprintf("Could not read line for %#v %s", id, err))
			return err
		}

		// append read bytes to line
		line = append(line, r...)

		// if it's a prefix more byes coming for this line so just continue the loop.
		// if not, then print the line and reset the line variable
		if !isPrefix {
			fmt.Printf("%s/%s: %s\n", aurora.Index(color, id.PodName), aurora.Index(color, id.ContainerName), string(line))
			line = make([]byte, 0)
		}

	}
}

func (k *KubeLogs) GetMultipleContainerLogs(ids []ContainerID, tail int64, follow bool) {
	for _, id := range ids {
		go k.GetContainerLogs(id, tail, follow)
	}
}

func (k *KubeLogs) LoggerIndex(id ContainerID) int {
	for index, active := range k.activeLoggers {
		if active == id {
			return index
		}
	}
	return -1
}

func (k *KubeLogs) LoggerActive(id ContainerID) bool {
	for _, active := range k.activeLoggers {
		if active == id {
			return true
		}
	}
	return false
}

func (k *KubeLogs) Debug(message string) {
	debug(message)
}

func New(kubeconfig string) *KubeLogs {
	k := &KubeLogs{}

	err := k.LoadConfigFromFile(kubeconfig)
	if err != nil {
		log.Fatalf("Could not load config: %s", err)
	}

	err = k.LoadClientSet()
	if err != nil {
		log.Fatalf("Could not load clientset: %s", err)
	}

	return k
}

type ContainerID struct {
	Namespace     string
	PodName       string
	ContainerName string
}

type PodList []v1.Pod

func (p PodList) GetContainerIds() []ContainerID {
	result := make([]ContainerID, 0)
	for _, pod := range p {
		for _, c := range pod.Status.ContainerStatuses {
			result = append(result, ContainerID{pod.GetNamespace(), pod.GetName(), c.Name})
		}
	}
	return result
}

func (p PodList) GetInitContainerIds() []ContainerID {
	result := make([]ContainerID, 0)
	for _, pod := range p {
		for _, c := range pod.Status.InitContainerStatuses {
			result = append(result, ContainerID{pod.GetNamespace(), pod.GetName(), c.Name})
		}
	}
	return result
}

func (p PodList) GetAllContainerIds() []ContainerID {
	result := make([]ContainerID, 0)
	result = append(result, p.GetContainerIds()...)
	result = append(result, p.GetInitContainerIds()...)
	return result
}

func (p PodList) FilterByName(filter string) PodList {
	result := make([]v1.Pod, 0)
	for _, pod := range p {

		match, err := regexp.MatchString(filter, pod.ObjectMeta.Name)
		if err != nil {
			return result
		}

		if match {
			result = append(result, pod)
		}

	}
	return result
}

func (p PodList) FilterByStarted() PodList {
	result := make([]v1.Pod, 0)
	for _, pod := range p {
		pod.Status.ContainerStatuses = (ContainerStatusList)(pod.Status.ContainerStatuses).FilterByStarted()
		pod.Status.InitContainerStatuses = (ContainerStatusList)(pod.Status.InitContainerStatuses).FilterByStarted()
		result = append(result, pod)
	}
	return result
}

type ContainerStatusList []v1.ContainerStatus

func (c ContainerStatusList) FilterByStarted() ContainerStatusList {
	result := make([]v1.ContainerStatus, 0)
	for _, containerStatus := range c {
		if containerStatus.State.Running != nil {
			result = append(result, containerStatus)
		}
	}
	return result
}
