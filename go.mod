module kubelogs

go 1.16

require (
	github.com/logrusorgru/aurora v2.0.3+incompatible
	k8s.io/api v0.20.5
	k8s.io/apimachinery v0.20.5
	k8s.io/client-go v0.20.5
)
