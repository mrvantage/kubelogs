package main

import (
	"flag"
	"kubelogs"
	"path/filepath"
	"time"

	"k8s.io/client-go/util/homedir"
)

func main() {

	var kubeconfig *string
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}

	var debug bool
	flag.BoolVar(&debug, "debug", false, "Enable kubelog internal debug logging")

	var interval int
	flag.IntVar(&interval, "interval", 5, "Poll k8s every n seconds for new pods / containers")
	flag.IntVar(&interval, "i", 5, "Poll k8s every n seconds for new pods / containers")

	var podFilter string
	flag.StringVar(&podFilter, "pod-filter", "", "Filter pod names (regex)")
	flag.StringVar(&podFilter, "p", "", "Filter pod names (regex)")

	var tail int64
	flag.Int64Var(&tail, "tail", 0, "Tail last n logs")
	flag.Int64Var(&tail, "t", 0, "Tail last n logs")

	var follow bool
	flag.BoolVar(&follow, "follow", true, "Follow future logs")
	flag.BoolVar(&follow, "f", true, "Follow future logs")

	flag.Parse()

	if debug {
		kubelogs.Debug = true
	}

	k := kubelogs.New(*kubeconfig)

	r := make(chan kubelogs.PodList)
	go k.WatchPods("", time.Second*time.Duration(interval), r)
	for {
		k.Debug("Waiting for result")
		t := <-r
		t = t.FilterByName(podFilter).FilterByStarted()
		k.GetMultipleContainerLogs(t.GetAllContainerIds(), tail, follow)
	}

}
